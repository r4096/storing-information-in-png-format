using System.Drawing;
using System.Drawing.Imaging;
using System.Text;

static class ConvertPNG
{
    /// <summary>
    /// Метод получения из png файла
    /// </summary>
    /// <param name="patchImage">Путь к PNG</param>
    /// <param name="k">Коэффициент увеличения пикселей</param>
    /// <returns>Массив в виде байтов</returns>
    public static byte[] PngToText(string patchImage, int k = 1)
    {
        byte[]? str = null;
        using (Bitmap btm = new Bitmap(patchImage))
        {

            int count = (btm.Width / k) * (btm.Height / k) * 3;

            str = new byte[count];

            count = 0;
            for (int x = 0; x < btm.Width / k; x++)
            {
                for (int y = 0; y < btm.Height / k; y++)
                {
                    str[count++] = btm.GetPixel(x * k, y * k).R;
                    str[count++] = btm.GetPixel(x * k, y * k).G;
                    str[count++] = btm.GetPixel(x * k, y * k).B;
                }
            }

        }
        return str;
    }

    /// <summary>
    /// Метод, который преобразует файлы в png файлы и помещает их в папку parts\
    /// </summary>
    /// <param name="path">Путь к папке с файлами</param>
    /// <param name="output">Папка назначения</param>
    /// <param name="k">Коэффициент увеличения пикселей</param>
    private static void ToMassPng(string path, string output, int k = 1)
    {
        string[] outp = output.Split('\\');
        StringBuilder strout = new StringBuilder();
        for (int c = 0; c < outp.Length - 1; c++)
            strout.Append(outp[c] + "\\");
        
        string[] files = Directory.GetFiles(path);
        for (int count_file = 1; count_file < files.Length; count_file++)
        {
            byte[] data = File.ReadAllBytes(files[count_file]);
            int i = 0;
            using (Bitmap bmp = new Bitmap(1920, 1080, PixelFormat.Format24bppRgb))
            {
                while (true)
                {
                    if (i >= data.Length)
                        break;
                    int r, g, b = 0;
                    for (int x = 0; x < bmp.Width; x += k)
                    {
                        for (int y = 0; y < bmp.Height; y += k)
                        {
                            r = (i >= data.Length) ? 0 : data[i];
                            g = (i >= data.Length - 1) ? 0 : data[i + 1];
                            b = (i >= data.Length - 2) ? 0 : data[i + 2];

                            for (int m = 0; m < k; m++)
                                for (int n = 0; n < k; n++)
                                    bmp.SetPixel(x + m, y + n, Color.FromArgb(r, g, b));
                            i = i + 3;
                        }
                    }
                    string export_name = count_file.ToString();
                    if (count_file < 10)
                        export_name = "00" + export_name;
                    else
                    if (count_file < 100)
                        export_name = "0" + export_name;
                    count_file++;
                    bmp.Save(strout.ToString() + @"parts\" + export_name + ".png", ImageFormat.Png);
                }
            }
        }
    }

    /// <summary>
    /// Массовое преобразование png в файлы в указанную папку
    /// </summary>
    /// <param name="path">Директория исходников</param>
    /// <param name="output">Директория исполнения</param>
    private static void ToGetMassPng(string path, string output, int k = 1)
    {
        string[] files = Directory.GetFiles(path);
        for (int count_file = 0; count_file < files.Length - 1; count_file++)
        {
            byte[] current = PngToText(files[count_file], k);
            using (var stream = new FileStream(output, FileMode.Append))
            {
                stream.Write(current, 0, current.Length);
            }
        }

        byte[] str = PngToText(files[files.Length - 1], k);
        int empty_count = 0;
        for (int count_byte = 1; count_byte <= str.Length; count_byte++)
            if (str[str.Length - count_byte] != 0)
            {
                empty_count = str.Length - count_byte + 1;
                break;
            }
        byte[] return_arr = new byte[empty_count];
        for (int i = 0; i < return_arr.Length; i++)
            return_arr[i] = str[i];
        using (var stream = new FileStream(output, FileMode.Append))
        {
            stream.Write(return_arr, 0, return_arr.Length);
        }
    }
}
